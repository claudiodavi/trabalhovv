/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leilaoonline;

import model.Item;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Claudio & Rodrigo
 */
public class ItemTest {

    Item it;

    public ItemTest() {
    }

    @Before
    public void setUp() {
        it = new Item();
    }

    @After
    public void tearDown() {
        it = null;
    }

    /**
     * Test of suplantarLance method, of class Item.
     */
    @Test
    public void testSuplantarLance() {
        double valor = 10;
        double novoValor = 15;

        it.setValorInicial(valor);
        it.suplantarLance(novoValor);
        
       Assert.assertTrue(it.getValorAtual() == novoValor);
    }
    
    @Test
    public void testSuplantarLanceNaoSuplanta(){
         double valor = 10.0;
        
        it.setValorAtual(valor);
        it.suplantarLance(5.0);
        double valorFinal = it.getValorAtual();
        
        Assert.assertEquals(valor, valorFinal, 0);
    }
  
}
