/*
 * The MIT License
 *
 * Copyright 2016 Claudio Souza.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package model;

/**
 *
 * @author Claudio & Rodrigo
 */
public class Item {

    private String titulo;
    private String descricao;
    private String imagem;
    private double valorInicial;
    private int tempoLeilao;
    private double valorAtual;
    private boolean ofertado;

    public Item() {

    }

    public Item(String ti, String desc, String img,
            double valorIni, int temp) {
        this.titulo = ti;
        this.imagem = img;
        this.descricao = desc;
        this.valorInicial = valorIni;
        this.tempoLeilao = temp;
        this.valorAtual = valorInicial;
    }

    public void suplantarLance(double novoValor) {
        if (getValorAtual() < novoValor) {
            setValorAtual(novoValor);
        }
    }

    public boolean isOfertado() {
        return ofertado;
    }

    public void setOfertado(boolean ofertado) {
        this.ofertado = ofertado;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the imagem
     */
    public String getImagem() {
        return imagem;
    }

    /**
     * @param imagem the imagem to set
     */
    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    /**
     * @return the valorInicial
     */
    public double getValorInicial() {
        return valorInicial;
    }

    /**
     * @param valorInicial the valorInicial to set
     */
    public void setValorInicial(double valorInicial) {
        this.valorInicial = valorInicial;
        this.valorAtual = valorInicial;
    }

    /**
     * @return the tempoLeilao
     */
    public int getTempoLeilao() {
        return tempoLeilao;
    }

    /**
     * @param tempoLeilao the tempoLeilao to set
     */
    public void setTempoLeilao(int tempoLeilao) {
        this.tempoLeilao = tempoLeilao;
    }

    /**
     * @return the valorAtual
     */
    public double getValorAtual() {
        return valorAtual;
    }

    /**
     * @param valorAtual the valorAtual to set
     */
    public void setValorAtual(double valorAtual) {
        this.valorAtual = valorAtual;
    }

    @Override
    public String toString() {
        return this.titulo;
    }
}
