/*
 * The MIT License
 *
 * Copyright 2016 davi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package persistencia;

import java.util.ArrayList;
import model.Cliente;
import model.Usuario;

/**
 *
 * @author Claudio & Rodrigo
 */
public class Usuarios {

    private ArrayList<Usuario> users = new ArrayList<Usuario>();
    private static Usuarios instance;

    public static Usuarios getInstance() {
        if (instance == null) {
            instance = new Usuarios();
        }
        return instance;
    }

    public Usuarios() {
        Cliente user1 = new Cliente();

        user1.setCpf("098765432");
        user1.setEmail("email@email.com");
        user1.setSenha("senha");
        user1.setUsuario("user1");
        user1.setTelefone("0988887767");
        /* Só pra ele ter um item na lista dele */
        user1.getItensOfertados().add(Itens.getInstance().getItens().get(0));

        Cliente user2 = new Cliente();
        user2.setCpf("09887788733");
        user2.setUsuario("user2");
        user2.setEmail("outro@email.com");
        user2.setSenha("senha");
        user2.setTelefone("0988898878");
        user2.getItensOfertados().add(Itens.getInstance().getItens().get(1));

        users.add(user1);
        users.add(user2);
    }

    public ArrayList<Usuario> getUsers() {
        return users;
    }
}
