/*
 * The MIT License
 *
 * Copyright 2016 Claudio Davi.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package persistencia;

import java.util.ArrayList;
import model.Item;

/**
 *
 * @author Claudio & Rodrigo
 */
public class Itens {

    private static Itens instance;

    public static Itens getInstance() {
        if (instance == null) {
            instance = new Itens();
        }
        return instance;
    }
    private ArrayList<Item> itens = new ArrayList<>();

    public Itens() {
        Item camisetaDoPele = new Item();
        camisetaDoPele.setTitulo("Camiseta do Pelé");
        camisetaDoPele.setDescricao("Camiseta Autografada");
        camisetaDoPele.setImagem("caminho/pra/imagem");
        camisetaDoPele.setOfertado(true);
        camisetaDoPele.setValorInicial(300.0);
        camisetaDoPele.setValorAtual(300.0);
        camisetaDoPele.setTempoLeilao(24);

        Item filmeDoPele = new Item();
        filmeDoPele.setTitulo("Filme do Pelé");
        filmeDoPele.setDescricao("Steve Jobs do futebol");
        filmeDoPele.setOfertado(true);
        filmeDoPele.setValorInicial(25.0);
        filmeDoPele.setValorAtual(25.0);
        filmeDoPele.setTempoLeilao(48);

        Item boneTrump = new Item();
        boneTrump.setTitulo("Boné Mr. Trump");
        boneTrump.setDescricao("MAKE AMERICA GREAT AGAIN");
        boneTrump.setOfertado(true);
        boneTrump.setValorAtual(15);
        boneTrump.setValorInicial(10);

        itens.add(camisetaDoPele);
        itens.add(filmeDoPele);
        itens.add(boneTrump);

    }

    public ArrayList<Item> getItens() {
        return itens;
    }

}
