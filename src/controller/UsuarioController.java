/*
 * The MIT License
 *
 * Copyright 2016 Claudio Souza.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package controller;

import javax.swing.DefaultListModel;
import model.Item;
import model.Lance;
import model.Usuario;
import persistencia.Usuarios;

/**
 *
 * @author Claudio & Rodrigo
 */
public class UsuarioController {

    Usuarios users = Usuarios.getInstance();

    public boolean fazLogin(String user, String password) {
        for (Usuario u : users.getUsers()) {
            if (u.getUsuario().equals(user) && u.getSenha().equals(password)) {
                return true;
            }
        }
        return false;
    }

    public DefaultListModel<String> lancesDoUsuario(Usuario u) {
        DefaultListModel<String> lances = new DefaultListModel<>();
        for (Lance l : u.getLances()) {
            lances.addElement(l.toString());
        }
        return lances;
    }

    public DefaultListModel<String> ofertasDoUsuario(Usuario u) {
        DefaultListModel<String> ofertas = new DefaultListModel<>();
        for (Item i : u.getItensOfertados()) {
            ofertas.addElement(i.getTitulo());
        }
        return ofertas;
    }

}
