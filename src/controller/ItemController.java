/*
 * The MIT License
 *
 * Copyright 2016 Claudio Souza.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package controller;

import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import model.Item;
import persistencia.Itens;

/**
 *
 * @author Claudio & Rodrigo
 */
public class ItemController {

    private Itens its = Itens.getInstance();

    public DefaultListModel<String> getItensTitulos() {
        DefaultListModel<String> itemTitulo = new DefaultListModel<>();

        its.getItens().stream().forEach((i) -> {
            itemTitulo.addElement(i.getTitulo());
        });
        return itemTitulo;
    }

    public ArrayList<Item> getItens() {
        return its.getItens();
    }

    public boolean createItem(Item it) {

        its.getItens().add(it);

        return true;
    }

    public boolean criaOfertaDialog() {
        JTextField titulo = new JTextField();
        JTextField descricao = new JTextField();
        JTextField preco = new JTextField();
        JTextField imagem = new JTextField();
        JTextField tempo = new JTextField();

        final JComponent[] inputs = new JComponent[]{
            new JLabel("Titulo"),
            titulo,
            new JLabel("Descrição"),
            descricao,
            new JLabel("Preço"),
            preco,
            new JLabel("Caminho para Imagem"),
            imagem,
            new JLabel("Tempo para Leilao"),
            tempo
        };

        int result = JOptionPane.showConfirmDialog(null, inputs,
                "Criar Oferta", JOptionPane.PLAIN_MESSAGE);

        if (result == JOptionPane.OK_OPTION) {
            createItem(new Item(titulo.getText(),
                    descricao.getText(),
                    imagem.getText(),
                    Double.parseDouble(preco.getText()),
                    Integer.parseInt(tempo.getText())));
            return true;
        } else {
            return false;
        }
    }

}
