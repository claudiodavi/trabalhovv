/*
 * The MIT License
 *
 * Copyright 2016 Claudio Souza.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package view;

import controller.ItemController;
import controller.LanceController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import model.Item;
import model.Usuario;

/**
 *
 * @author Claudio & Rodrigo
 */
public class MainView extends javax.swing.JFrame {

    LanceController lcCtrl = new LanceController();
    ItemController itCtrl = new ItemController();
    Item itemSelected;

    public MainView(Usuario user) {
        initComponents();
        ItemList.setModel(itCtrl.getItensTitulos());
        perfil.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PerfilView pview = new PerfilView(user);
                pview.setVisible(true);
                pview.setTitle("Perfil");
                pview.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            }
        });

        ItemList.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                carregaNaTela(e);
            }

        });
        lance.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                realizaLance(user);
            }
        });
        oferta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (itCtrl.criaOfertaDialog()) {
                    ItemList.setModel(itCtrl.getItensTitulos());
                }
            }
        });

    }

    private void carregaNaTela(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting()) {
            itemSelected = itCtrl.getItens().get(ItemList.getSelectedIndex());
            atualPrice.setText(String.valueOf(itemSelected.getValorAtual()));
            desc.setText(itemSelected.getDescricao());
        }
    }

    private void realizaLance(Usuario us) {
        if (!offerPrice.getText().isEmpty()
                && offerPrice.getText().matches("\\d+")) {
            double precoOferecido = lcCtrl.parsingPrecos(offerPrice.getText());
            double novoPreco = lcCtrl.realizarLance(itemSelected, precoOferecido, us);
            offerPrice.setText("");

            JOptionPane.showMessageDialog(null, "Lance registrado com sucesso"
                    + "\nValor Atual: " + itemSelected.getValorAtual());
            atualPrice.setText(String.valueOf(novoPreco));
        }
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        ItemList = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        offerPrice = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        lance = new javax.swing.JButton();
        atualPrice = new javax.swing.JLabel();
        imgProduto = new javax.swing.JPanel();
        desc = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        options = new javax.swing.JMenu();
        perfil = new javax.swing.JMenuItem();
        oferta = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        ItemList.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        ItemList.setToolTipText("");
        jScrollPane1.setViewportView(ItemList);

        jLabel1.setText("Preço Atual");

        offerPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                offerPriceActionPerformed(evt);
            }
        });

        jLabel2.setText("Ofertar:");

        lance.setText("Realizar Lance");

        desc.setText("descrição");

        javax.swing.GroupLayout imgProdutoLayout = new javax.swing.GroupLayout(imgProduto);
        imgProduto.setLayout(imgProdutoLayout);
        imgProdutoLayout.setHorizontalGroup(
            imgProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(imgProdutoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(desc, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE))
        );
        imgProdutoLayout.setVerticalGroup(
            imgProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, imgProdutoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(desc)
                .addContainerGap())
        );

        options.setText("Opções");

        perfil.setText("Perfil");
        options.add(perfil);

        oferta.setText("Ofertar Item");
        oferta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ofertaActionPerformed(evt);
            }
        });
        options.add(oferta);

        jMenuBar1.add(options);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lance, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel1)
                                .addComponent(jLabel2))
                            .addGap(18, 18, 18)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(atualPrice, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
                                .addComponent(offerPrice))))
                    .addComponent(imgProduto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(11, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(imgProduto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(atualPrice))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(offerPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(7, 7, 7)
                        .addComponent(lance))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void ofertaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ofertaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ofertaActionPerformed

    private void offerPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_offerPriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_offerPriceActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JList<String> ItemList;
    private javax.swing.JLabel atualPrice;
    private javax.swing.JLabel desc;
    private javax.swing.JPanel imgProduto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton lance;
    private javax.swing.JMenuItem oferta;
    private javax.swing.JTextField offerPrice;
    private javax.swing.JMenu options;
    private javax.swing.JMenuItem perfil;
    // End of variables declaration//GEN-END:variables

}
